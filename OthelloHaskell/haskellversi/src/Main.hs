{-#LANGUAGE InstanceSigs#-} -- Permit type declarations in instance definitions

module Main where
 -- Fixed-size arrays indexed by an Ix instance
import Data.Array
 -- Haskell doesn't have a NULL type per se, so Maybe can be used to describe a result that may not have a definite value
import Data.Maybe
 -- Folds/recursive combining of some foldable entities (e.g lists, sets) to one
import Data.Foldable
 -- Our graphics library
import Graphics.Gloss
 -- We need access to events, so we use the game mode.
import Graphics.Gloss.Interface.Pure.Game

type UnitScore = Int
type Coordinate = (Int, Int)

-- A player is either a Red or a Blue. Derive default comparison and show
data Player = Red | Blue deriving (Eq, Show)

-- Define the state of each spot on a board; either it is empty, or it may have a player's button placed on it.
data BoardPosition = Empty | Placed Player deriving (Eq, Show)

-- Define a simple model for a board; an array indexed by 2-dimensional coordinates and containing board positions.
data Board = Board {
    boardGrid :: Array Coordinate (BoardPosition) -- Implicitly create a function called 'boardGrid', which extracts the grid array itself from a Board value
}  



-- Score type for a board score. A board is Win, if it is a certain win for a given player, and Lose if it is a certain loss. If it is neither, it is Indeterminate, with a score denoting its "goodness"
data BoardScore = Win | Indeterminate UnitScore | Lose deriving (Eq, Show)

-- Define a order for a board score. For least complexity, define ordering as a set of comparative properties between different scores
instance Ord BoardScore where
    (<=) :: BoardScore -> BoardScore -> Bool
    (<=) Lose _ = True -- Lose is the smallest and definitely equal
    (<=) (Indeterminate _) Lose = False -- Indeterminate is never less or equal to a win
    (<=) (Indeterminate _) Win = True -- Indeterminate is always less than a win
    (<=) (Indeterminate a) (Indeterminate b) = (a <= b) -- For two indeterminates, their respective ordering depends on their scores
    (<=) Win Win = True -- Win is equal with a win
    (<=) Win _ = False -- Otherwise, no

-- Grid size
gameGridSize :: Int
gameGridSize = 8

-- Starting pieces, where appropriate. First coordinate is X, second Y
startPieces :: (Int, Int) -> BoardPosition
startPieces (3,3) = Placed Red
startPieces (4,4) = Placed Red
startPieces (3,4) = Placed Blue
startPieces (4,3) = Placed Blue
startPieces _ = Empty -- If no other coordinate matches, it is an empty square.

-- Which turns the AI plays? Empty list means humans play both turns. It is also permissible to have the AI play both turns
aiPlays :: [Player]
aiPlays = [Blue]

-- How many turns the AI is approximately allowed to analyze?
-- The time taken for a search should be approximately constant; larger the amount, more turns the AI can take to determine the best option, and therefore more time is spent.
aiSearchDepth = 3000

-- Returns a count of pieces on a board - red first, blue second
pieceCount :: Board -> (Int, Int)
pieceCount board = foldr (counter) (0,0) (elems (boardGrid board)) -- Recursively add the score together position by position  
    where
        -- A function to define how the total score changes per position found
        counter :: BoardPosition -> (Int, Int) -> (Int, Int)
        counter pos (red, blue) = case (pos) of
                                    Empty -> (red, blue) -- No change
                                    Placed Red -> (red+1, blue) -- One more for red
                                    Placed Blue -> (red, blue+1) -- One more for blue

-- Calculate the winner using the traditional rules - who has most pieces, wins. If we can not determine one, return Nothing
winningPlayer :: Board -> Maybe Player
winningPlayer board
    | draw = Nothing
    | otherwise = if redCount > blueCount then Just Red else Just Blue
    where
        draw = (blueCount == redCount)
        (redCount, blueCount) = pieceCount board


-- Function that gets a piece from a coordinate
pieceAtCoordinate :: Board -> Coordinate -> BoardPosition
pieceAtCoordinate board coordinate = (boardGrid board) ! coordinate 

-- Function that checks if a given coordinate is within the given board
coordinateInBounds :: Board -> Coordinate -> Bool
coordinateInBounds board coord = inRange (bounds (boardGrid board)) coord

-- Define an initial board
initialBoard = Board (array ((0,0), (gameGridSize-1, gameGridSize-1)) (gridComprehension (\point -> (point, startPieces point))))

-- A helper function for grid comprehension - map some function over the game grid       
gridComprehension :: (Coordinate -> x) -> [x]
gridComprehension func = [(func (a,b)) | a <- [0..gameGridSize-1], b <- [0..gameGridSize-1]]       

-- Definition of the opposing player for a given player
opposingPlayer :: Player -> Player
opposingPlayer Red = Blue
opposingPlayer Blue = Red

-- Returns a list of pieces that should be changed to the player's color on a board when clicking on some point. If the list is empty, the move is not valid
getMovesOnPoint :: Board -> Player -> Coordinate -> [Coordinate]
getMovesOnPoint board player base_coord
     -- Not a valid base coordinate, invalid
    | isValidBaseCoord == False = []
     -- Not valid, must be an empty location
    | basePiece /= (Empty) = []
     -- No valid directions, no result
    | null (resultingDirections) = []
     -- Include our base coord, and return
    | otherwise = base_coord:resultingDirections
    where
        resultingDirections = concat (map (walkAndMark) directionsToCheck)

        walkAndMark :: Coordinate -> [Coordinate]
        walkAndMark direction = walkAndMarkIntr (baseX+dirX,baseY+dirY) direction [] where
            (dirX,dirY) = direction

        -- Walk and mark - walk in a direction and mark down the found coordinates. If it terminates on a placed piece of the opposing player, return the list. If to our player or empty, nothing
        walkAndMarkIntr :: Coordinate -> Coordinate -> [Coordinate] -> [Coordinate]
        walkAndMarkIntr currentPos direction listOfFound
            -- At the end, do not include the terminating piece
            | isEndPiece = listOfFound
            | isValidTraversalPiece = walkAndMarkIntr (curX+dirX,curY+dirY) direction (currentPos:listOfFound)
            -- Not a valid end piece nor a traversal piece
            | otherwise = [] 
            where
                (curX,curY) = currentPos
                (dirX,dirY) = direction
                isEndPiece = isValidPos && ((pieceAtCoordinate board currentPos) == (Placed player))
                isValidTraversalPiece = isValidPos && ((pieceAtCoordinate board currentPos) == (Placed (opposingPlayer player)))
                isValidPos = (coordinateInBounds board currentPos)
         -- Which relative directions we need to check?
        directionsToCheck = [(-1,-1), (-1,0), (-1,1), (0,-1), (0,1), (1,-1), (1,0), (1,1)]

        (baseX,baseY) = base_coord
        isValidBaseCoord = coordinateInBounds board base_coord
        basePiece = pieceAtCoordinate board base_coord 

-- Returns a list containing lists of coordinates for applying moves. The first item in the list is always the piece clicked
movesAvailableForPlayer :: Board -> Player -> [[Coordinate]]
movesAvailableForPlayer board player = filteredResults -- Return the resulting list as defined below
    where
        filteredResults = filter (\lst -> (null lst) == False) mappedCoords
        mappedCoords = gridComprehension (getMovesOnPoint board player)

-- Determines if no moves are possible at all on a given board
noMovesPossibleAtAll :: Board -> Bool
noMovesPossibleAtAll board = null $ (movesAvailableForPlayer board Red) ++ (movesAvailableForPlayer board Blue)

-- Applies a move; in practice, this means setting the pieces at coordinates given to the player wanted         
applyMove :: Board -> Player -> [Coordinate] -> Board
applyMove board player move_list = Board ((boardGrid board) // (map (\coord -> (coord, Placed player)) move_list))

-- Get's the AI's choice of a move
getAIsMove :: Board -> Player -> [Coordinate]
getAIsMove board main_player = case (moves) of
    [] -> []
    _ -> getBestMove moves
    where
        moves = movesAvailableForPlayer board main_player
    
        getBestMove mvs = fst (maximumBy (\a b -> compare (snd a) (snd b)) $ (map (\move -> (move, (getNestedScore (applyMove board main_player move) (opposingPlayer main_player) (aiSearchDepth - (length moves)) False))) mvs))
        -- Calculates a nested score. This is a classic Minimax algorithm for decisionmaking
        getNestedScore :: Board -> Player -> Int -> Bool -> BoardScore
        getNestedScore brd plr depth_allowed maximizing
             -- If this is a game-over scenario, or we are out of moves, the terminal score is a must 
            | gameAtEnd || (futureNestedScore < 0) = terminalScore
             -- If we cannot move forward, change the turn and look from the other party's viewpoint
            | (null currentPlrMoves) = getNestedScore brd (opposingPlayer plr) (futureNestedScore) (not maximizing)
             -- If we want to maximize our score, get the maximum score available
            | maximizing = maximum (Lose:(map (\move -> getNestedScore (applyMove brd plr move) (opposingPlayer plr) (futureNestedScore) (not maximizing)) currentPlrMoves))
             -- On the other hand, if we want the least good score for the player whose score we should minimize, calculate that here
            | otherwise = minimum (Win:(map (\move -> getNestedScore (applyMove brd plr move) (opposingPlayer plr) (futureNestedScore) (not maximizing)) currentPlrMoves))
            where
                futureNestedScore = if (length currentPlrMoves == 0) then (depth_allowed-1) else (depth_allowed - (length currentPlrMoves)) `div` (length currentPlrMoves) -- Innovate - subtract the amount of further turns, and then allocate the rest for nested processing
            
                terminalScore -- Terminal score for our main player; this way, minimization and maximization always have a reasonable result
                     -- Endgame, at this point we know the wins and the losses
                    | gameAtEnd, redCount /= blueCount = if (main_player == Red && (redCount > blueCount)) then Win else Lose
                    | otherwise = Indeterminate ((if (main_player == Red) then 1 else -1) * (redCount-blueCount))
                
                (redCount,blueCount) = pieceCount brd
                
                 -- Is this game at its end? Enforce that 
                gameAtEnd = (null (currentPlrMoves)) && (null (opposingPlrMoves)) && plr == main_player
                -- Moves for both parties
                currentPlrMoves = movesAvailableForPlayer brd plr
                opposingPlrMoves = movesAvailableForPlayer brd (opposingPlayer plr)


-- The resolution of the window
resolutionX :: Int
resolutionX = 700
resolutionY :: Int
resolutionY = 700

-- As the rendering is from the centre, in what way the coordinates should be translated to return them back to left lower edge-based positioning
centreAdjustmentX :: Int
centreAdjustmentX = -1 * (resolutionX `div` 2)
centreAdjustmentY :: Int
centreAdjustmentY = -1 * (resolutionY `div` 2)

gridAbsoluteLeftX :: Int
gridAbsoluteLeftX = 50 -- From left edge
gridAbsoluteLeftY :: Int
gridAbsoluteLeftY = 100 -- From bottom
gridBoxSize :: Int
gridBoxSize = 45 -- How large a single square is?

circleSize :: Float
 -- Radius of the circle
circleSize = 20

-- From which coordinate the text is rendered, X
textBottomLeftX = 50 
-- From which coordinate the text is rendered, Y 
textBottomLeftY = 100  

{- GUI code starts from here-}
data GameWorld = World {
    gameBoard :: Board, -- Board in this current state?
    playerTurn :: Player, -- Whose turn it is
    passedOnLastTurn :: Bool, -- Was there a pass on the last turn?
    bothStalled :: Bool, -- Has the game stalled, AKA two passes in a row, meaning game over
    ticks :: Float -- Ticks counter to count how long to wait until AI kicks into action
}

-- Initial world contains an initial board state, starting on Red, no passes or stall and starting at zero ticks.
initialWorld = World initialBoard Red False False 0.0 

-- Tries to locate the coordinates the mouse did click. If available, return Just it, otherwise Nothing
getClickTarget :: (Float, Float) -> Maybe Coordinate
getClickTarget (clickX, clickY)
    | dividedX < 0 || dividedX >= gameGridSize = Nothing -- Invalid coordinates
    | dividedY < 0 || dividedY >= gameGridSize = Nothing
    
    | otherwise = Just (dividedX, dividedY)
    
    where
        -- dividedX, dividedY should directly correspond to grid coordinates
        dividedX :: Int
        -- Div is an integer division; Haskell is remarkably strict about types, so we need to explicitly accept the loss of precision associated
        dividedX = (translatedClickX) `div` gridBoxSize 
        dividedY :: Int
        dividedY = (gameGridSize-1) - (translatedClickY `div` gridBoxSize)
        translatedClickX :: Int
        translatedClickX = (round clickX) - centreAdjustmentX - gridAbsoluteLeftX
        translatedClickY :: Int
        translatedClickY = (round clickY) - centreAdjustmentY - gridAbsoluteLeftY

-- Rendering is a bit tricky, as the render is centered to the center of the window and not to the sides! We then need to adjust these down
renderWorld :: GameWorld -> Picture
renderWorld world = Translate (fromIntegral centreAdjustmentX) (fromIntegral centreAdjustmentY) (Pictures (concat [boardRender, gridList, [textRender]])) where

    winningPlayerText :: Maybe Player -> String
    winningPlayerText (Just Red) = "Red wins"
    winningPlayerText (Just Blue) = "Blue wins"
    winningPlayerText Nothing = "Draw"

    textRender = Scale 0.15 0.15 $ (Translate textBottomLeftX textBottomLeftY $ Color white (Text (
                case () of _
                            | noMovesPossibleAtAll (gameBoard world) -> winningPlayerText (winningPlayer (gameBoard world))
                            | null (movesAvailableForPlayer (gameBoard world) (playerTurn world)) -> "No possible positions for you, " ++ (show $ playerTurn world) ++ ", passing"
                            | playerTurn (world) == Blue -> "Turn for Blue"
                            | playerTurn (world) == Red -> "Turn for Red"
                            | otherwise -> ""
                )))


    -- Board state
    boardRender = [Translate (fst $ gridPosForAssoc placedButton) (snd $ gridPosForAssoc placedButton) $ Color (colorForAssoc placedButton) (Circle circleSize) | placedButton <- placedButtons]
        where 
            gridPosForAssoc :: (Coordinate, BoardPosition) -> (Float, Float)
            gridPosForAssoc assoc = (actualX, actualY) where
                 (coordX, coordY) = fst assoc

                 actualX :: Float
                 actualX = fromIntegral $ gridAbsoluteLeftX + ((coordX*gridBoxSize) + (gridBoxSize `div` 2))
                 actualY :: Float
                 actualY = fromIntegral $ gridAbsoluteLeftY + ((((gameGridSize-1)-(coordY))*gridBoxSize) + (gridBoxSize `div` 2))


            colorForAssoc :: (Coordinate, BoardPosition) -> Color
            colorForAssoc assoc = if (snd assoc) == Placed Red then red else blue
            placedButtons = filter (\asc -> snd (asc) /= Empty) (assocs (boardGrid (gameBoard world)))


    -- List comprehension to form the drawings for the grid
    gridDrawingFunction (gridX, gridY) = whiteBox (fromIntegral $ gridAbsoluteLeftX+(gridBoxSize*gridX),fromIntegral $ gridAbsoluteLeftY + (gridBoxSize*gridY)) (fromIntegral $ gridAbsoluteLeftX+(gridBoxSize*(gridX+1)),fromIntegral $ gridAbsoluteLeftY+(gridBoxSize*(gridY+1)))
    gridList = gridComprehension gridDrawingFunction

    -- A helper function for rendering a white box
    whiteBox startPoint endPoint = Color white (Line points) where
        (endX, endY) = endPoint
        (startX, startY) = startPoint
        points = [(startX, startY), (endX, startY), (endX, endY), (startX, endY), (startX, startY)]


handleEvent :: Event -> GameWorld -> GameWorld
handleEvent (EventKey (MouseButton RightButton) Down _ _) _ = initialWorld
handleEvent (EventKey (MouseButton LeftButton) Down _ clickPos) world
    -- Both players have passed, so the game's over
    | bothStalled world = world 
    --  AI plays this turn
    | elem (playerTurn world) (aiPlays) = world
    -- No valid position
    | Nothing <- possibleClickPos = world 
    -- We have a position,  evaluate it
    | Just coordinate <- possibleClickPos = evaluatePlayerTurn world coordinate
    where
        evaluatePlayerTurn :: GameWorld -> Coordinate -> GameWorld
        evaluatePlayerTurn wrld crd
            -- No valid moves
            | null (moveOnPoint) = wrld
            -- Apply a move and change the turn to the opposing player; also reset any pass counters and the ticker
            | otherwise = World (appliedBoard) (opposingPlayer (playerTurn wrld)) False False 0 
            where
                appliedBoard = applyMove (gameBoard wrld) (playerTurn wrld) moveOnPoint
                moveOnPoint = getMovesOnPoint (gameBoard wrld) (playerTurn wrld) crd 
        possibleClickPos = getClickTarget clickPos
-- Rest do not affect the world
handleEvent _ world = world 

timerTick :: Float -> GameWorld -> GameWorld
timerTick tick_diff (World board turn passedOnLast bothStalled curTicks)
    | tick_diff+curTicks < 1.0 = World board turn passedOnLast bothStalled (curTicks+tick_diff)
    | otherwise = evaluateTickTurn 
    where
        evaluateTickTurn = case () of _
                                         -- Both have stalled, do not do anything
                                        | bothStalled -> tickResetWorld
                                         -- Pass, unable to make a turn
                                        | null (movesAvailableForPlayer board turn) -> World board (opposingPlayer turn) True (passedOnLast) 0
                                         -- AI does not play this turn
                                        | notElem turn (aiPlays) -> tickResetWorld
                                        | otherwise -> World (applyMove board turn possibleAIturn) (opposingPlayer turn) False False 0
        
        -- No changes apart from the ticks resetting on tickResetWorld
        tickResetWorld = World board turn passedOnLast bothStalled 0
        possibleAIturn = getAIsMove board turn

main = play -- This program is a GLOSS game..
        (InWindow "Haskellversi" (resolutionX,resolutionY) (20,20)) -- In a window of a suitable size
         black -- With a black background
         24 -- 24fps
         initialWorld -- Initial state
         renderWorld -- Render images using renderWorld
         handleEvent -- Handle events using handleEvent
         timerTick -- Timer effects via timerTick..
