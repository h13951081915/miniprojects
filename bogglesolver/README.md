# bogglesolver

## Info

A simple Boggle solver program. When a grid is entered, it presents all solutions it can find. Included is a small test dictionary.

## On compatibility

__It is highly advisable to use Linux; the program was tested there, and this program is not Windows compatible in this form__; what Windows users need to do:

- Remove Readline from dependencies and substitute it with something else, or alterntively install it (author of this program wasn't able to do it easily)
- Ensure that the input is intepreted properly, as misintepreted multibyte characters can easily break the dictionary
- Possibly change output symbols to be compatible with the console; it seems that at least on Windows 7, special Unicode characters are not well supported in console output.

This may not be as straightforward as it seems. Still, Happy Boggling!
